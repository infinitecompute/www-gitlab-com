---
layout: markdown_page
title: "FY20-Q4 OKRs"
---

This fiscal quarter will run from November 1, 2019 to January 31, 2020.

## On this page
{:.no_toc}

- TOC
{:toc}

### 1. CEO: IACV.


### 2. CEO: Popular next generation product.


### 3. CEO: Great team.
