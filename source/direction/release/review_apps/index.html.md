---
layout: markdown_page
title: "Category Vision - Review Apps"
---

- TOC
{:toc}

## Review Apps

Review Apps let you build a review process right into your software development workflow
by automatically provisioning test environments for your code, integrated right
into your merge requests.

This area of the product is in need of continued refinement to add more kinds of
review apps (such as for mobile devices), and a smoother, easier to use experience.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AReview%20Apps)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/ci/review_apps/)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1299) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Next up we will provide several mechanisms to make it easier to clean up environments. We 
understand that environments tend to grow exponentially and it is a burden to mane them: 
* Extending the API to provide an ability to run scripts to delete environments [gitlab-ee#5582](https://gitlab.com/gitlab-org/gitlab-ee/issues/5582)
* Adding an expiration time for environments [gitlab-ee#42822](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822)
* Provide a mechanism to clean stale environments [gitlab-ee#38718](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718)

## Maturity Plan

This category is currently at the "Complete" maturity level, and
our next maturity target is Lovable (see our [definitions of maturity levels](/direction/maturity/#maturity-plan)).
Key deliverables to achieve this are:
- [Mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718)
- [Optional expiration time for environments](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822)
- [Truly dynamic environment URLs](https://gitlab.com/gitlab-org/gitlab-ce/issues/27424)
- [First-class review apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/35849)

## Competitive Landscape

One big advantage Heroku Review Apps have over ours is that they are easier to set up
and get running. Ours require a bit more knowledge and reading of documentation to make
this clear. We can make our Review Apps much easier (and thereby much more visible) by
implementing [gitlab-ce#27893](https://gitlab.com/gitlab-org/gitlab-ce/issues/27893),
which does the heavy lifting of getting them working for you.

## Top Customer Success/Sales Issue(s)

Management of Review Apps can be a challenge, particularly in cleaning them up. To
highlight the severity of how this issue can grow, www-gitlab-com project has over
1,500 running stale environments at the time of writing this, with no clear easy way
to clean them up. There are two main items that look to address this challenge:

- [gitlab-ce#42822](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822) builds in an expiration date for review apps, beyond which they will automatically be terminated.
- [gitlab-ce#38718](https://gitlab.com/gitlab-org/gitlab-ce/issues/38718) (also the #2 customer issue) implements a way to clean up environments that either did not have an expiration date or were not terminated for other reasons.
## Top Customer Issue(s)

The top customer issue impacting users of Review Apps is  [gitlab-ee#42822](https://gitlab.com/gitlab-org/gitlab-ce/issues/42822)
which sets an expiration date for an environment. 
## Top Internal Customer Issue(s)

In some cases after a review app is not availiable for a specific MR  
([gitlab-ee#10733](https://gitlab.com/gitlab-org/gitlab-ee/issues/10733)), adding some 
information to the user as to what action needs to be done in order to resolve this and
taker action directly from the MR itself, would help not only our customers, 
but also our own developers. 
## Top Vision Item(s)

Our focus for the vision is to bring Review Apps to mobile workflows via
[gitlab-ce#40683](https://gitlab.com/gitlab-org/gitlab-ce/issues/40683) - adding
support to Android/iOS emulators via the Review App will enable a whole new kind
of development workflow in our product, and make Review Apps even more valuable.
